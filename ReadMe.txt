Created by Priyank Kabra (priyank.kabra@cleartrip.com)

JSON objects format examples for post mapping methods

url -> "localhost:8100/addItem"
JSON object ->    {
                       "id": 10,
                       "name": "Cough_Syrup",
                       "price": 10.59,
                       "itemCategories": {
                           "name": "Medical_Products"
                       }
                   }

url -> "localhost:8100/addCategory"
JSON object -> {
                       "name": "Electronics",
                       "salesTax": 0.15
                   }

url -> "localhost:8100/newBasket"
JSON object -> {
                       "id" : 123
                   }

url -> "localhost:8100/addItemsToBasket"
JSON object -> {
               	"id" : 4,
               	"quantity" : 1,
               	"isImported" : true,
               	"item" : {
               		"id" : 9
               	},
               	"basket" : {
               		"id" : 123
               	}
               }