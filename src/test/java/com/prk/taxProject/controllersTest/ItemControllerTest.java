package com.prk.taxProject.controllersTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prk.taxProject.controllers.ItemController;
import com.prk.taxProject.model.Item;
import com.prk.taxProject.model.ItemCategories;
import com.prk.taxProject.services.ItemService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.Charset;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ItemController.class)
public class ItemControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ItemService itemService;

    @Test
    public void getAllItemsTest() throws Exception
    {
        RequestBuilder request = MockMvcRequestBuilders.get("/showItems")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        //String resultContent = result.getResponse().getContentAsString();
        //System.out.println(resultContent);

        Mockito.verify(itemService, Mockito.times(1)).getAllItems();
    }

    @Test
    public void addItemTest() throws Exception
    {
        Item item = new Item(100, "Bike", 10000.55, new ItemCategories("Vehicles"));

        System.out.println("\n\n" + item);

        String jsonRequest = objectMapper.writeValueAsString(item);

        RequestBuilder request = MockMvcRequestBuilders.post("/addItem")
                .content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .contentType("application/json; charset=UTF-8");

        MvcResult result = mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
}