package com.prk.taxProject.controllersTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prk.taxProject.controllers.ItemCategoriesController;
import com.prk.taxProject.model.ItemCategories;
import com.prk.taxProject.services.ItemCategoriesService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ItemCategoriesController.class)
public class ItemCategoriesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ItemCategoriesService itemCategoriesService;

    @Test
    public void getAllCategoriesTest() throws Exception
    {
        RequestBuilder request = MockMvcRequestBuilders.get("/showCategories")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(request).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        Mockito.verify(itemCategoriesService, Mockito.times(1)).getAllCategories();
    }

    @Test
    public void addCategoryTest() throws Exception
    {
        ItemCategories itemCategory = new ItemCategories("Food", 0.0);

        String jsonRequest = objectMapper.writeValueAsString(itemCategory);

        RequestBuilder request = MockMvcRequestBuilders.post("/addCategory")
                .content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
                .contentType("application/json; charset=UTF-8")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
}
