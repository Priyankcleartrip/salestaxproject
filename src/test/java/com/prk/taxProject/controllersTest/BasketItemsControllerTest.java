package com.prk.taxProject.controllersTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prk.taxProject.controllers.BasketItemsController;
import com.prk.taxProject.model.Basket;
import com.prk.taxProject.model.BasketItems;
import com.prk.taxProject.model.Item;
import com.prk.taxProject.services.BasketItemsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BasketItemsController.class)
public class BasketItemsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BasketItemsService basketItemsService;

    @Test
    public void getAllBasketItemsTest() throws Exception
    {
        RequestBuilder request = MockMvcRequestBuilders.get("/showBasketItems")
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(request).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        Mockito.verify(basketItemsService, Mockito.times(1)).getAllBasketItems();
    }

    @Test
    public void generatBillTest() throws Exception
    {
        RequestBuilder request = MockMvcRequestBuilders.get("/bill/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        Mockito.verify(basketItemsService, Mockito.times(1)).generateBill(2);
    }

    @Test
    public void addItemTest() throws Exception
    {
        BasketItems basketItems = new BasketItems(1, new Item(100),true,6, new Basket(123));

        String jsonRequest = objectMapper.writeValueAsString(basketItems);

        RequestBuilder request = MockMvcRequestBuilders.post("/addItemToBasket")
                .content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
                .contentType("application/json; charset=UTF-8")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
}
