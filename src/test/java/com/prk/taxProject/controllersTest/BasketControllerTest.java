package com.prk.taxProject.controllersTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prk.taxProject.controllers.BasketController;
import com.prk.taxProject.model.Basket;
import com.prk.taxProject.services.BasketService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BasketController.class)
public class BasketControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BasketService basketService;

    @Test
    public void getAllBasketsTest() throws Exception
    {
        RequestBuilder request = MockMvcRequestBuilders.get("/showBaskets")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(request).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        Mockito.verify(basketService, Mockito.times(1)).getAllBaskets();
    }

    public void createBasketTest() throws Exception
    {
        Basket basket = new Basket(1234);

        String jsonRequest = objectMapper.writeValueAsString(basket);

        RequestBuilder request = MockMvcRequestBuilders.post("/newBasket")
                .content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
                .contentType("application/json; charset=UTF-8")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
}