package com.prk.taxProject.servicesTest;

import com.prk.taxProject.model.ItemCategories;
import com.prk.taxProject.repositories.ItemCategoriesRepository;
import com.prk.taxProject.services.ItemCategoriesService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ItemCategoriesService.class})
public class ItemCategoriesServiceTest {

    @Autowired
    private ItemCategoriesService itemCategoriesService;

    @MockBean
    private ItemCategoriesRepository itemCategoriesRepository;

    @Test
    public void getAllCategoriesTest()
    {
        Mockito.when(itemCategoriesRepository.findAll()).thenReturn(Stream.of(new ItemCategories("Electronics", 1.5),
                new ItemCategories("Vehicles", 2.0)).collect(Collectors.toList()));

        Assertions.assertEquals(2, itemCategoriesService.getAllCategories().size());
    }

    @Test
    public void addCategoryTest()
    {
        ItemCategories itemCategory = new ItemCategories("Others", 0.1);
        itemCategoriesService.addCategory(itemCategory);

        Mockito.verify(itemCategoriesRepository, Mockito.times(1)).save(itemCategory);
    }
}
