package com.prk.taxProject.servicesTest;

import com.prk.taxProject.model.Basket;
import com.prk.taxProject.repositories.BasketRepository;
import com.prk.taxProject.services.BasketService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {BasketService.class})
public class BasketServiceTest {

    @Autowired
    private BasketService basketService;

    @MockBean
    private BasketRepository basketRepository;

    @Test
    public void getAllBaskets()
    {
        Mockito.when(basketRepository.findAll()).thenReturn(Stream.of(new Basket(100),
                new Basket(200), new Basket(300)).collect(Collectors.toList()));

        Assertions.assertEquals(3, basketService.getAllBaskets().size());
    }

    @Test
    public void createBasket()
    {
        Basket basket = new Basket(123);
        basketService.createBasket(basket);

        Mockito.verify(basketRepository, Mockito.times(1)).save(basket);
    }
}
