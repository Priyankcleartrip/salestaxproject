package com.prk.taxProject.servicesTest;

import com.prk.taxProject.model.Item;
import com.prk.taxProject.model.ItemCategories;
import com.prk.taxProject.repositories.ItemRepository;
import com.prk.taxProject.services.ItemService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ItemService.class})
public class ItemServiceTest {

    @MockBean
    private ItemRepository itemRepository;

    @Autowired
    private ItemService itemService;

    @Test
    public void getAllItemsTest()
    {
        Mockito.when(itemRepository.findAll()).thenReturn(Stream.of(new Item(100,
                "Watch", 123.45, new ItemCategories("Others")),
                new Item(200, "Eye Drops", 50.25,
                        new ItemCategories("Medical Products"))).collect(Collectors.toList()));

        Assertions.assertEquals(2, itemService.getAllItems().size());
    }

    @Test
    public void addItemTest()
    {
        Item item = new Item(123, "Bike", 10000.50, new ItemCategories("Vehicle"));
        itemService.addItem(item);

        Mockito.verify(itemRepository, Mockito.times(1)).save(item);
    }
}