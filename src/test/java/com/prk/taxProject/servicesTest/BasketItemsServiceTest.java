package com.prk.taxProject.servicesTest;

import com.prk.taxProject.model.Basket;
import com.prk.taxProject.model.BasketItems;
import com.prk.taxProject.model.Item;
import com.prk.taxProject.repositories.BasketItemsRepository;
import com.prk.taxProject.services.BasketItemsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {BasketItemsService.class})
public class BasketItemsServiceTest {

    @Autowired
    private BasketItemsService basketItemsService;

    @MockBean
    private BasketItemsRepository basketItemsRepository;

    @Test
    public void getAllBasketItemsTest()
    {
        Mockito.when(basketItemsRepository.findAll()).thenReturn(Stream.of(new BasketItems(
                1, new Item(4), true, 5, new Basket(50)), new BasketItems(
                5, new Item(9), false, 2, new Basket(98)
        )).collect(Collectors.toList()));

        Assertions.assertEquals(2, basketItemsService.getAllBasketItems().size());
    }

    @Test
    public void taxRoundOffTest()
    {
        Assertions.assertAll(
                () -> Assertions.assertEquals(0.0, basketItemsService.taxRoundOff("0.00")),
                () -> Assertions.assertEquals(5.8, basketItemsService.taxRoundOff("5.76")),
                () -> Assertions.assertEquals(1.35, basketItemsService.taxRoundOff("1.32")),
                () -> Assertions.assertEquals(9.0, basketItemsService.taxRoundOff("8.97"))
        );
    }

    @Test
    public void addItemTest()
    {
        BasketItems basketItem = new BasketItems(1, new Item(4),
                true, 5, new Basket(50));

        basketItemsService.addItem(basketItem);

        Mockito.verify(basketItemsRepository, Mockito.times(1)).save(basketItem);
    }
}
