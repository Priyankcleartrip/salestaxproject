package com.prk.taxProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 Created by Priyank Kabra (priyank.kabra@cleartrip.com)
*/

@SpringBootApplication
public class TaxProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaxProjectApplication.class, args);
	}
}