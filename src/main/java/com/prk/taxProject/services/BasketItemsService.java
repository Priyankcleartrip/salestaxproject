package com.prk.taxProject.services;

import com.prk.taxProject.model.BasketItems;
import com.prk.taxProject.repositories.BasketItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Service
/* Service class for basket items */
public class BasketItemsService {

    @Autowired
    private BasketItemsRepository basketItemsRepository;

    /* returns list of items in all the baskets */
    public List<BasketItems> getAllBasketItems()
    {
        return ((ArrayList<BasketItems>)(basketItemsRepository.findAll()));
    }

    /* Adds an item to a particular basket */
    public void addItem(BasketItems basketItems)
    {
        basketItemsRepository.save(basketItems);
    }

    /* Round Up method to round up tax to the nearest 0.05 decimal format */
    public Double taxRoundOff(String taxAmount)
    {
        /* Logic -
        1) Decimal number received to the method in String format
        2) It always has two decimal places only
        3) Checked least significant decimal digit
            3a) if digit is 0 or 5 then return as it was,
            3b) if digit is between 1-4 them make it 5,
            3c) if digit is greater than 5 then make it 0 and then add 0.1 to the number to round it up.
         4) return the decimal number after parsing
        */

        String taxRoundedOff = taxAmount.substring(0, ((taxAmount.length()) - 1));
        char leastSignificantDigit = taxAmount.charAt((taxAmount.length()) -1);

        double taxValue = 0.0;

        switch(leastSignificantDigit)
        {
            case '1':
            case '2':
            case '3':
            case '4':
                taxRoundedOff += "5";
                taxValue = Double.parseDouble(taxRoundedOff);
                break;

            case '6':
            case '7':
            case '8':
            case '9':
                taxRoundedOff += "0";
                taxValue = Double.parseDouble(taxRoundedOff);
                taxValue += + 0.10;
                break;

            default:
                taxValue = Double.parseDouble(taxAmount);
        }

        return taxValue;
    }

    /* returns a list of String that acts as final bill for a particular order id */
    public List<String> generateBill(int orderId)
    {
        /* list that is going to be returned */
        ArrayList<String> bill = new ArrayList<String>();

        /* list of all items in a particular basket */
        List<BasketItems> itemsInBasket = basketItemsRepository.getBasketItems(orderId);

        /* used to calculate tax on each item */
        double taxOnOneItem = 0.0;

        /* stores final sales tax amount */
        double totalTax = 0.0;

        /* stores final price of item after sales tax applied to it */
        double priceOfOneItem = 0.0;

        /* stores sum total of all prices and taxes of items in basket */
        double totalAmount = 0.0;

        /* used to calculate sales tax for each item */
        double taxMultiplier = 0.0;

        /* stores one by one Item objects from the list */
        BasketItems oneItem = null;

        /* used to format the data upto two decimal places */
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        decimalFormat.setRoundingMode(RoundingMode.UP);

        /* traversing through the list of items in basket */
        for(int i=0; i<itemsInBasket.size(); i++)
        {
            /* stored bill string for a particular item
            and then will be added to the arrayList of bill created above */
            String oneItemBillStatement = "";

            oneItem = itemsInBasket.get(i);
            taxMultiplier = ((oneItem.getItem()).getItemCategories()).getSalesTax();

            /* imported items has 5% more sales tax
                So, sales tax multiplier will be added 5% i.e. 0.05 */
            if(oneItem.getIsImported())
                taxMultiplier += 0.05;

            /* tax = tax multipler * quantity * price of item */
            taxOnOneItem = (taxMultiplier * ((oneItem.getQuantity()) * ((oneItem.getItem()).getPrice())));
            taxOnOneItem = taxRoundOff(decimalFormat.format(taxOnOneItem));

            totalTax += taxOnOneItem;

            /* price after tax = (shelf price * quantity) + tax */
            priceOfOneItem = taxOnOneItem + (((oneItem.getItem()).getPrice()) * (oneItem.getQuantity()));
            totalAmount += priceOfOneItem;

            /* creating bill statement
             example -> 1 imported bottle of perfume : 32.19 */
            oneItemBillStatement = oneItemBillStatement + (oneItem.getQuantity()) + " ";

            if(oneItem.getIsImported())
                oneItemBillStatement = oneItemBillStatement + "imported" + " ";

            oneItemBillStatement = oneItemBillStatement + ((oneItem.getItem()).getName()) + " : " +
                    decimalFormat.format(priceOfOneItem);

            bill.add(oneItemBillStatement);
        }

        bill.add("Sales Taxes : " + decimalFormat.format(totalTax));
        bill.add("Total : " + decimalFormat.format(totalAmount));

        return bill;
    }
}