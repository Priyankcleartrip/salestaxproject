package com.prk.taxProject.services;

import com.prk.taxProject.model.Basket;
import com.prk.taxProject.repositories.BasketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
/* Service class for basket */
public class BasketService {

    @Autowired
    private BasketRepository basketRepository;

    /* returns list of all baskets */
    public List<Basket> getAllBaskets()
    {
        return ((ArrayList<Basket>)(basketRepository.findAll()));
    }

    /* creates and adds new basket to database */
    public void createBasket(Basket basket)
    {
        basketRepository.save(basket);
    }
}