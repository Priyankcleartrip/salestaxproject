package com.prk.taxProject.services;

import com.prk.taxProject.model.Item;
import com.prk.taxProject.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
/* Service class for Item */
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

    /* returns list of all items available in database */
    public List<Item> getAllItems()
    {
        return ((ArrayList<Item>)(itemRepository.findAll()));
    }

    /* adds a new item to database */
    public void addItem(Item item)
    {
        itemRepository.save(item);
    }
}