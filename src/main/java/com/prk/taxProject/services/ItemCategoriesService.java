package com.prk.taxProject.services;

import com.prk.taxProject.model.ItemCategories;
import com.prk.taxProject.repositories.ItemCategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
/* Service class for item categories */
public class ItemCategoriesService {

    @Autowired
    private ItemCategoriesRepository itemCategoriesRepository;

    /* returns list of all item categories */
    public List<ItemCategories> getAllCategories()
    {
        return ((ArrayList<ItemCategories>)(itemCategoriesRepository.findAll()));
    }

    /* creates and adds a new item category in database */
    public void addCategory(ItemCategories itemCategories)
    {
        itemCategoriesRepository.save(itemCategories);
    }
}