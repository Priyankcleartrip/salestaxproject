package com.prk.taxProject.controllers;

import com.prk.taxProject.model.BasketItems;
import com.prk.taxProject.services.BasketItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
/* Controller class for Basket items */
public class BasketItemsController {

    @Autowired
    private BasketItemsService basketItemsService;

    /* returns list of items in all the baskets when url is opened */
    @GetMapping("/showBasketItems")
    public List<BasketItems> getAllBasketItems()
    {
        return basketItemsService.getAllBasketItems();
    }

    /* Adds an item to a particular basket when url is opened
     Object of BasketItems is passed in request body whose data is stored in database */
    @PostMapping("/addItemToBasket")
    public void addItem(@RequestBody BasketItems basketItems)
    {
        basketItemsService.addItem(basketItems);
    }

    /* returns the bill of a particular basket when url is opened
      basket id is passed alongwith url */
    @GetMapping("/bill/{id}")
    public List<String> generateBill(@PathVariable("id") String orderId)
    {
        return basketItemsService.generateBill(Integer.parseInt(orderId));
    }
}
