package com.prk.taxProject.controllers;

import com.prk.taxProject.model.Item;
import com.prk.taxProject.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
/* Controller class for Item */
public class ItemController {

    @Autowired
    private ItemService itemService;

    /* returns a list of items when url is opened */
    @GetMapping("/showItems")
    public List<Item> getAllItems()
    {
        return itemService.getAllItems();
    }

    /* Adds an item when url is opened
     Object of Item is passed in request body whose data is stored in database */
    @PostMapping("/addItem")
    public void addItem(@RequestBody Item item)
    {
        itemService.addItem(item);
    }
}