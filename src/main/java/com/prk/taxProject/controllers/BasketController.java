package com.prk.taxProject.controllers;

import com.prk.taxProject.model.Basket;
import com.prk.taxProject.services.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
/* Controller class for Basket */
public class BasketController {

    @Autowired
    private BasketService basketService;

    /* returns list of all baskets when url is opened */
    @GetMapping("/showBaskets")
    public List<Basket> getAllBaskets()
    {
        return basketService.getAllBaskets();
    }

    /* creates and adds new basket when url is opened
        Object of Basket is passed in request body whose data is stored in database */
    @PostMapping("/newBasket")
    public void createBasket(@RequestBody Basket basket)
    {
        basketService.createBasket(basket);
    }
}
