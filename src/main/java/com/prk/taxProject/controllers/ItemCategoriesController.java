package com.prk.taxProject.controllers;

import com.prk.taxProject.model.ItemCategories;
import com.prk.taxProject.services.ItemCategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
/* Controller class for item categories */
public class ItemCategoriesController {

    @Autowired
    private ItemCategoriesService itemCategoriesService;

    /* returns list of all item categories when url is opened */
    @GetMapping("/showCategories")
    public List<ItemCategories> getAllCategories()
    {
        return itemCategoriesService.getAllCategories();
    }

    /* Adds an item category when url is opened
      Object of itemCategories is passed as request body whose data is stored in database */
    @PostMapping("/addCategory")
    public void addCategory(@RequestBody ItemCategories itemCategories)
    {
        itemCategoriesService.addCategory(itemCategories);
    }
}