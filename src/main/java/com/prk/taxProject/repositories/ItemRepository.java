package com.prk.taxProject.repositories;

import com.prk.taxProject.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

/* For database operations in items table */
public interface ItemRepository extends JpaRepository<Item, Integer> {
}
