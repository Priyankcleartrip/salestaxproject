package com.prk.taxProject.repositories;

import com.prk.taxProject.model.ItemCategories;
import org.springframework.data.jpa.repository.JpaRepository;

/* For database operations in item_categories table */
public interface ItemCategoriesRepository extends JpaRepository<ItemCategories, String> {
}
