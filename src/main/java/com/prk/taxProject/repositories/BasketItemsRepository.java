package com.prk.taxProject.repositories;

import com.prk.taxProject.model.BasketItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/* For database operations in basket_items table */
public interface BasketItemsRepository extends JpaRepository<BasketItems, Integer> {

    /* returns list of all items that belong to a particular basket_id */
    @Query(value = "select * from basket_items where order_id = ?1", nativeQuery = true)
    public List<BasketItems> getBasketItems(int orderId);
}
