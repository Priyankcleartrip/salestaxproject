package com.prk.taxProject.repositories;

import com.prk.taxProject.model.Basket;
import org.springframework.data.jpa.repository.JpaRepository;

/* For database operations in basket table */
public interface BasketRepository extends JpaRepository<Basket, Integer> {
}
