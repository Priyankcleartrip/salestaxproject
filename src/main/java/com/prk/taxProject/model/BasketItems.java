package com.prk.taxProject.model;

import javax.persistence.*;

@Entity
@Table(name = "basket_items")
/* basket_items table stores order items information in database
    It has 5 columns -> id, isImported, quantity, item_id, order_id */
public class BasketItems {

    @Id
    @Column(name = "id")
    /* new id is created when an item is added to basket
        JPA repository requires a primary key otherwise no need of this variable */
    private int id;

    @Column(name = "is_imported")
    /* stores whether item is imported or not */
    private boolean isImported;

    @Column(name = "quantity")
    /* stores quantity of a particular item added in basket */
    private int quantity;

    @OneToOne
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    /* refering to items table "item_id" will act as foreign key in this table */
    private Item item;

    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    /* refering to basket table "order_id" will act as foreign key in this table */
    private Basket basket;

    public BasketItems(int id, Item item, boolean isImported, int quantity, Basket basket) {
        this.id = id;
        this.item = item;
        this.isImported = isImported;
        this.quantity = quantity;
        this.basket = basket;
    }

    public BasketItems() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public boolean getIsImported() {
        return isImported;
    }

    public void setIsImported(boolean isImported) {
        this.isImported = isImported;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }
}