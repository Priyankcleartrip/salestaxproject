package com.prk.taxProject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "item_categories")
/* item_categories table stores all the item categories information
    (example -> food, books, others)
     It has 2 columns -> name, sales_tax */
public class ItemCategories {

    @Id
    @Column(name = "name")
    /* stores name of a category */
    private String name;

    @Column(name = "sales_tax")
    /* stores sales tax multiplier
        example -> 0.1 , means an item of 50 will have tax of (50 * 0.1 = 5) */
    private double salesTax;

    @JsonIgnore
    @OneToMany(mappedBy = "itemCategories")
    /* category "name" acts as foreign key in items table */
    private List<Item> items = new ArrayList<Item>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalesTax() {
        return salesTax;
    }

    public void setSalesTax(double salesTax) {
        this.salesTax = salesTax;
    }

    public ItemCategories() {
    }

    public ItemCategories(String name, double salesTax) {
        this.name = name;
        this.salesTax = salesTax;
    }

    public ItemCategories(String name) {
        this.name = name;
    }
}