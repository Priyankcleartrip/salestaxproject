package com.prk.taxProject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "basket")
/* basket table stores all order id in database
    It has only one column -> id */
public class Basket {

    @Id
    @Column(name = "id")
    /* stores order_id */
    private int id;

    @JsonIgnore
    @OneToMany(mappedBy = "basket")
    /* id will act as foreign key in basket_items table */
    private List<BasketItems> basketItems = new ArrayList<BasketItems>();

    public int getId() {
        return id;
    }

    public Basket() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public Basket(int id) {
        this.id = id;
    }
}
