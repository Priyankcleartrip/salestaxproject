package com.prk.taxProject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "items")
/* item table stores data about a particular item in database
  It has 4 columns -> id, name, price, category */
public class Item {

    @Id
    @Column(name = "id")
    /* stores item_id for a particular item */
    private int id;

    @Column(name = "name")
    /* stores name of a particular item */
    private String name;

    @Column(name = "price")
    /* stores price of a particular item */
    private double price;

    @ManyToOne
    @JoinColumn(name = "category", referencedColumnName = "name")
    /* refering to item_categories table "category" acts as foreign key in this table */
    private ItemCategories itemCategories;

    @JsonIgnore
    @OneToOne(mappedBy = "item")
    /* item_id acts as foreign key in basket_items table */
    private BasketItems basketItems;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ItemCategories getItemCategories() {
        return itemCategories;
    }

    public void setItemCategories(ItemCategories itemCategories) {
        this.itemCategories = itemCategories;
    }

    public Item() {
    }

    public Item(int id, String name, double price, ItemCategories itemCategories) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.itemCategories = itemCategories;
    }

    public Item(int id) {
        this.id = id;
    }
}