insert into item_categories (name, sales_tax) values
('Food', 0.00), ('Books', 0.00), ('Medical_Products', 0.00), ('Others', 0.1);

insert into items (id, name, price, category) values
(1, 'Book', 12.49, 'Books'), (2, 'Music_CD', 14.99, 'Others'), (3, 'Chocolate_Bar', 0.85, 'Food'),
(4, 'Box_Of_Chocolates', 10.00, 'Food'), (5, 'Bottle_Of_Perfume', 47.50, 'Others'),
(6, 'Bottle_Of_Perfume', 27.99, 'Others'), (7, 'Bottle_Of_Perfume', 18.99, 'Others'),
(8, 'Packet_Of_Headache_Pills', 9.75, 'Medical_Products'), (9, 'Box_Of_Chocolates', 11.25, 'Food');